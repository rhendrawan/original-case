KLM Deal Finder
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

```
# start the frontend build
./ng build

# run the backend engine
./gradlew bootRun
```

to list all backend tasks:

`./gradlew tasks`

To view the result after starting the application go to:

[http://localhost:9000/](http://localhost:9000)

