package com.afkl.cases.df.service.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

public class CoordinatesTest {

    @Test
    public void testObjectMapper() throws Exception {
        String jsonString = "{\n" +
          "                    \"latitude\": 49.00972,\n" +
          "                    \"longitude\": 2.54861\n" +
          "                }";
        ObjectMapper mapper = new ObjectMapper();
        Coordinates coordinates = mapper.readValue(jsonString, Coordinates.class);
        assert(coordinates != null);
    }

}
