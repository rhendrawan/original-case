package com.afkl.cases.df.service.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

public class LocationTest {

    @Test
    public void testObjectMapper() throws Exception {
        String jsonString = "{\n" +
                "    \"code\": \"FR\",\n" +
                "    \"name\": \"France\",\n" +
                "    \"description\": \"France (FR)\",\n" +
                "    \"coordinates\": {\n" +
                "    \"latitude\": 46.0,\n" +
                "    \"longitude\": 2.0\n" +
                "  }\n" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        Location location = mapper.readValue(jsonString, Location.class);
        assert (location != null);
    }

}
