package com.afkl.cases.df.service;

import com.afkl.cases.df.service.model.Fare;
import com.afkl.cases.df.service.model.Location;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.TypeReferences;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DealFinderService {

  private final OAuth2RestTemplate restTemplate;
  private final String backendUri;

  public DealFinderService(OAuth2RestTemplate restTemplate,
                           MeterRegistry registry,
                           @Value("${backend.client.uri}") String backendUri) {
    this.restTemplate = restTemplate;
    this.backendUri = backendUri;
  }

  public PagedResources<Resource<Location>> getAirports(final String searchTerm, Integer page, Integer size, String language) {

    Map<String, Object> params = new HashMap<>();
    params.put("term", searchTerm);
    params.put("lang", language);
    params.put("page", page);
    params.put("size", size);

    List<String> queryParams = params.entrySet().stream()
      .map(e -> String.format("%s=%s", e.getKey(), e.getValue()))
      .collect(Collectors.toList());

    final String targetUri = String.format("%s/airports?%s", backendUri, String.join("&", queryParams));
    ResponseEntity<PagedResources<Resource<Location>>> resp =
      restTemplate.exchange(targetUri, HttpMethod.GET, null,
        new TypeReferences.PagedResourcesType<Resource<Location>>(){}, params);

    return resp.getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
  }

  public Location getAirport(String airportCode) {
    final String targetUri = String.format("%s/airports/%s", backendUri, airportCode);
    ResponseEntity<Location> resp = restTemplate.getForEntity(targetUri, Location.class, Collections.emptyMap());
    return resp.getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
  }

  public Fare getFare(String originCode, String destinationCode, String currency) {
    final String targetUri = String.format("%s/fares/%s/%s?currency=%s", backendUri, originCode, destinationCode, currency);
    ResponseEntity<Fare> resp = restTemplate.getForEntity(targetUri, Fare.class, Collections.emptyMap());
    return resp.getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
  }

}
