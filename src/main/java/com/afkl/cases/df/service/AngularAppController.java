package com.afkl.cases.df.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AngularAppController {

    // Specific angular routing rules should be mapped to index.html
    @RequestMapping(value = {"/", "/statistics/**", "/instructions/**", "airport-list/**", "help/**", "imprint/**"})
    public String index() {
        return "index.html";
    }

}
