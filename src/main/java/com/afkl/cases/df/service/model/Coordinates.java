package com.afkl.cases.df.service.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class Coordinates {

    private double latitude, longitude;

    @JsonCreator
    public static Coordinates create(@JsonProperty("latitude") double latitude,
                                     @JsonProperty("longitude") double longitude) {
        return new Coordinates(latitude, longitude);
    }

}
