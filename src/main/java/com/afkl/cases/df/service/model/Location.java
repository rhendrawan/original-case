package com.afkl.cases.df.service.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@AllArgsConstructor
@JsonInclude(NON_NULL)
@Value
public class Location {

    private String code, name, description;
    private Coordinates coordinates;
    private Location parent;
    private Set<Location> children;

    @JsonCreator
    public static Location create(@JsonProperty("code") String code,
                                  @JsonProperty("name") String name,
                                  @JsonProperty("description") String description,
                                  @JsonProperty("coordinates") Coordinates coordinates,
                                  @JsonProperty("parent") Location parent,
                                  @JsonProperty("children") Set<Location> children) {
        return new Location(code, name, description, coordinates, parent, children);
    }

}
