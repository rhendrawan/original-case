package com.afkl.cases.df.service;

import com.afkl.cases.df.service.model.Fare;
import com.afkl.cases.df.service.model.Location;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Timed
public class DealFinderController {

  @Autowired
  private DealFinderService service;

  @RequestMapping("/airports")
  public PagedResources<Resource<Location>> searchAirports(
          @RequestParam(value="term") String searchTerm,
          @RequestParam(value="page", required = false, defaultValue = "1") Integer page,
          @RequestParam(value="size", required = false, defaultValue = "5") Integer size,
          @RequestParam(value="lang", required = false, defaultValue = "en") String language) {
    return service.getAirports(searchTerm, page, size, language);
  }

  @RequestMapping("/airports/{code}")
  public Location getAirport(@PathVariable(value="code") String airportCode) {
    return service.getAirport(airportCode);
  }

  @RequestMapping("/fares/{origin}/{destination}")
  public Fare getFare(@PathVariable(value="origin") String originCode,
                      @PathVariable(value="destination") String destinationCode,
                      @RequestParam(value="currency", required = false, defaultValue = "EUR") String currency) {
    return service.getFare(originCode, destinationCode, currency);
  }


}
