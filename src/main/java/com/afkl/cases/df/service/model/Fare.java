package com.afkl.cases.df.service.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;


@AllArgsConstructor
@Value
public class Fare {

    Double amount;
    Currency currency;
    String origin, destination;

    @JsonCreator
    public static Fare create(@JsonProperty("amount") Double amount,
                              @JsonProperty("currency") Currency currency,
                              @JsonProperty("origin") String origin,
                              @JsonProperty("destination") String destination) {
        return new Fare(amount, currency, origin, destination);
    }

}

