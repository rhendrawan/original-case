package com.afkl.cases.df.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableOAuth2Client
public class DealFinderConfiguration {

  private static final List<MediaType> HAL_TYPES = Arrays.asList(MediaTypes.HAL_JSON, MediaTypes.HAL_JSON_UTF8);

  @Bean
  public OAuth2RestTemplate oauth2RestTemplate(
    @Value("${backend.oauth2.clientId}") String oAuth2ClientId,
    @Value("${backend.oauth2.clientSecret}") String oAuth2ClientSecret,
    @Value("${backend.oauth2.accessTokenUri}") String accessTokenUri)
  {
    ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();

    details.setId("1");
    details.setTokenName("OAuth2Token");
    details.setClientId(oAuth2ClientId);
    details.setClientSecret(oAuth2ClientSecret);
    details.setAccessTokenUri(accessTokenUri);
    details.setScope(Arrays.asList("read", "write", "trust"));

    OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(details, new DefaultOAuth2ClientContext());
    restTemplate.setMessageConverters(Traverson.getDefaultMessageConverters(HAL_TYPES));
    return restTemplate;
  }

}
