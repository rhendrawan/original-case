import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchBarComponent } from './search-bar.component';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpClientModule, NgbModule ],
  declarations: [ SearchBarComponent ],
  exports: [ SearchBarComponent ],
  bootstrap: [ SearchBarComponent ]
})

export class SearchBarModule {}
