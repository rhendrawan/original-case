import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';

import { Location, Fare } from '../models/location.model';
import { BackendService } from '../services/backend.service';
import { DispatcherService } from "../services/dispatcher.service";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  providers: [BackendService],
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {

  origin: any;
  destination: any;

  fareText: string;
  errorMessage: string;

  loading = false;
  dealFound = false;

  originInvalid = false;
  destinationInvalid = false;

  constructor(private backend: BackendService,
              private dispatcher: DispatcherService,
              private router: Router,
              private route: ActivatedRoute) {}

  ngOnInit() {
    let originCode = this.route.snapshot.queryParams['origin'];
    if (originCode) {
      this.backend.locate(originCode).subscribe(origin => {
        this.selectOrigin(origin);
      });
    }
    let destinationCode = this.route.snapshot.queryParams['destination'];
    if (destinationCode) {
        this.backend.locate(destinationCode).subscribe(destination => {
          this.selectDestination(destination)
        });
    }
  }

  search = (text$: Observable<string>, setInvalid: (invalid : boolean) => any) => {
    return text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.loading = true;
        this.errorMessage = "";
      }),
      switchMap(term =>
        this.backend.search(term).pipe(
          tap(() => setInvalid(false)),
          map(result => result.locations),
          catchError(e => {
            setInvalid(true);
            this.errorMessage = "Could not find results for " + term + ".";
            return of([]);
          })
        )
      ),
      tap(() => this.loading = false)
    )
  }

  formatInput = (result: Location) => result.parent.name + " (" + result.code + "), " + result.parent.parent.name;

  formatTypeahead = (result: Location) => result.description;

  searchOrigin = (text$: Observable<string>) => {
     return this.search(text$, (invalid: boolean) => { this.originInvalid = invalid});
  }

  searchDestination = (text$: Observable<string>) => {
     return this.search(text$, (invalid: boolean) => { this.destinationInvalid = invalid});
  }

  selectOrigin(origin: Location){
    this.origin = origin;
    this.dispatcher.publishOrigin(origin);
    this.updateQueryParameter('origin', origin.code);
    this.findDeal(origin, this.destination);
  }

  selectDestination(destination: Location){
    this.destination = destination;
    this.dispatcher.publishDestination(destination);
    this.updateQueryParameter('destination', destination.code);
    this.findDeal(this.origin, destination);
  }

  updateQueryParameter(key: string, value: string) {
      this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {[key]: value},
          queryParamsHandling: 'merge'
      });
  }

  findDeal(origin: Location, destination: Location) {
    if (origin && destination && origin.coordinates && destination.coordinates) {
        this.loading = true;
        this.fareText = "";
        this.errorMessage = "";
        // Fetch fare from backend in the background
        this.backend.getFare(origin, destination).subscribe(
          fare => {
            this.fareText = fare.amount + " " + fare.currency;
            this.loading = false;
          },
          error => {
            this.errorMessage = "Could not find deal for " + origin.code + " - " + destination.code +".";
          }
        );
    }
  }

}

