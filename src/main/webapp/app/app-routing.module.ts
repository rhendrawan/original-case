import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DisplayMapComponent } from './display-map/display-map.component';
import { AirportListComponent } from './airport-list/airport-list.component';

const routes: Routes = [
  { path: 'statistics', component: DashboardComponent },
  { path: 'airport-list', component: AirportListComponent },
  { path: '',   component: DisplayMapComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
