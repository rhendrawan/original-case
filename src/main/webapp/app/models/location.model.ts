
export class Coordinates {
    latitude: number;
    longitude: number;
}

export class Location {
    code: string;
    name: string;
    description: string;
    coordinates: Coordinates;
    parent: Location;
}

export class Fare {
    origin: string;
    destination: string;
    amount: number;
    currency: string;
}