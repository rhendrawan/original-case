
export class Measurement {
    statistic: string;
    value: number;
}

export class MetricTag {
    tag: string;
    values: string[];
}

export class Metrics {
    name: string;
    description: string;
    baseUnit: string;
    measurements: Measurement[];
    availableTags: MetricTag[];
    parent: Location;
}
