import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppConfigModule } from './app-config.module';
import { AppRoutingModule } from './app-routing.module';
import { AirportListModule } from './airport-list/airport-list.module';
import { SearchBarModule } from './search-bar/search-bar.module';
import { DashboardModule } from './dashboard/dashboard.module';

import { TopBarComponent } from './top-bar/top-bar.component';
import { DisplayMapComponent } from './display-map/display-map.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { AppComponent } from './app.component';

import { BackendService } from './services/backend.service';
import { DispatcherService } from "./services/dispatcher.service";
import { MetricsService } from './services/metrics.service';

@NgModule({
  declarations: [
    TopBarComponent,
    DisplayMapComponent,
    AppComponent,
    BottomBarComponent
  ],
  imports: [
    BrowserModule,
    AppConfigModule,
    AppRoutingModule,
    SearchBarModule,
    AirportListModule,
    DashboardModule
  ],
  providers: [
    BackendService,
    DispatcherService,
    MetricsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
