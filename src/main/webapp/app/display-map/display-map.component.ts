import { Component, Directive, OnInit, ViewChild, ElementRef, Input, Inject, Injectable } from '@angular/core';
import { DispatcherService } from "../services/dispatcher.service";
import { BackendService } from "../services/backend.service";

import { APP_CONFIG, AppConfig } from '../app-config.module';

import { tap } from 'rxjs/operators';

import { Location, Coordinates } from '../models/location.model';

const AIRPORT_ICON = 'assets/airport.svg';

declare var H: any;

@Component({
    selector: 'app-display-map',
    templateUrl: './display-map.component.html',
    providers: [BackendService, DispatcherService],
    styleUrls: ['./display-map.component.scss']
})
@Injectable()
export class DisplayMapComponent implements OnInit {

    @ViewChild("map", {static: false})
    public mapElement: ElementRef;

    private platform: any;
    private map: any;

    private markerGroup : any;
    private objects : any;

    private origin: Location;
    private destination: Location;

    public constructor(
        private backend: BackendService,
        private dispatcher: DispatcherService,
        @Inject(APP_CONFIG) private config: AppConfig
    ) { }

    public ngOnInit() {
      this.dispatcher.currentOrigin.subscribe(origin => {
        this.origin = origin;
        this.redrawPoints(this.origin, this.destination);
      });
      this.dispatcher.currentDestination.subscribe(destination => {
        this.destination = destination;
        this.redrawPoints(this.origin, this.destination);
      });
    }

    public ngAfterViewInit() {

      // HERE map platform
      this.platform = new H.service.Platform({
          "app_id": this.config.appId,
          "app_code": this.config.appCode,
          "useHTTPS": true
      });

      let pixelRatio = window.devicePixelRatio || 1;
      let defaultLayers = this.platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320
      });

      this.map = new H.Map(
          this.mapElement.nativeElement,
          defaultLayers.normal.base,
          {
              center: {lat:52, lng:5}, // default view and zoom is in Europe
              zoom: 5,
              pixelRatio: pixelRatio
          }
      );

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => this.map.getViewPort().resize());

      // Global grouping to keep track of map view bounds.
      this.markerGroup = new H.map.Group();
      this.map.addObject(this.markerGroup);

      // For all custom objects to be removed when performing reset.
      this.objects = [];
    }

    redrawPoints(origin: Location, destination: Location) {
      if (origin.coordinates && destination.coordinates) {
        this.reset();
        this.drawFlightPath(origin, destination);
        this.drawMarker(origin);

        let target = this.drawMarker(destination);
      }
    }

    reset() {
      // clear all past objects, for reloaded plans page
      this.markerGroup.removeAll();
      this.map.removeObjects(this.objects);

      // reset object list
      this.objects = [];
    }

    getLatLng(coordinates: Coordinates) {
      return { lat: coordinates.latitude, lng: coordinates.longitude };
    }

    drawFlightPath = function(origin: Location, destination: Location) {
      let lineString = new H.geo.LineString();
      let self = this;

      [origin, destination].forEach(function (location) {
        lineString.pushPoint(self.getLatLng(location.coordinates));
      });

      // draw the lines with appropriate styling
      let lineStyle = { lineWidth: 4, strokeColor: 'blue' };
      let arrowStyle = {width: 6, length: 10, frequency: 18, fillColor: lineStyle.strokeColor};
      let polyline = new H.map.Polyline(lineString, {style: lineStyle, arrows: arrowStyle});

      this.map.addObject(polyline);
      this.objects.push(polyline);
    }

    drawMarker = function(location: Location) {
      let latLng = this.getLatLng(location.coordinates);
      let marker = new H.map.Marker(latLng);
      marker.setIcon(new H.map.Icon(AIRPORT_ICON, {size: {w: 40, h: 40}}));
      marker.setData('<div>' + location.name + '<img src="assets/loading-blue.gif" /></div>');

      this.markerGroup.addObject(this.getMarkerLabel(latLng, location.code));
      this.markerGroup.addObject(marker);

      this.map.setViewBounds(this.markerGroup.getBounds());
      return marker;
    }

    getMarkerLabel = function(latLng, labelText) {
      var outerElement = document.createElement('div'),
          innerElement = document.createElement('div');

      outerElement.style.userSelect = 'none';
      outerElement.style.webkitUserSelect = 'none';
      outerElement.style.msUserSelect = 'none';
      outerElement.style.cursor = 'default';

      // add negative margin to inner element
      // to move the anchor to center of the div
      innerElement.style.marginTop = '-20px';
      innerElement.style.marginLeft = '10px';

      outerElement.appendChild(innerElement);

      // Add text to the DOM element
      innerElement.innerHTML = labelText;

      function changeOpacity(evt) {
        evt.target.style.opacity = 0.6;
      };

      function changeOpacityToOne(evt) {
        evt.target.style.opacity = 1;
      };

      //create dom icon and add/remove opacity listeners
      var domIcon = new H.map.DomIcon(outerElement, {
        // the function is called every time marker enters the viewport
        onAttach: function(clonedElement, domIcon, domMarker) {
          clonedElement.addEventListener('mouseover', changeOpacity);
          clonedElement.addEventListener('mouseout', changeOpacityToOne);
        },
        // the function is called every time marker leaves the viewport
        onDetach: function(clonedElement, domIcon, domMarker) {
          clonedElement.removeEventListener('mouseover', changeOpacity);
          clonedElement.removeEventListener('mouseout', changeOpacityToOne);
        }
      });

      // Marker for Chicago Bears home
      return new H.map.DomMarker(latLng, {icon: domIcon});
    }

}
