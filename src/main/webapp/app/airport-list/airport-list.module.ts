import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AirportListComponent } from './airport-list.component';
import { AirportListDirective } from './airport-list.directive';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  declarations: [ AirportListComponent, AirportListDirective ],
  exports: [ AirportListComponent ],
  bootstrap: [ AirportListComponent ]
})

export class AirportListModule {}
