import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';
import { AirportListDirective, SortEvent } from './airport-list.directive';

import { Location } from '../models/location.model';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'app-airport-list',
  providers: [LocationService, DecimalPipe],
  templateUrl: './airport-list.component.html',
  styleUrls: ['./airport-list.component.scss']
})
export class AirportListComponent implements OnInit {

  airports$: Observable<Location[]>;
  total$: Observable<number>;

  @ViewChildren(AirportListDirective)
  headers: QueryList<AirportListDirective>;

  constructor(public service: LocationService) {
    this.airports$ = service.locations$;
    this.total$ = service.total$;
  }

  ngOnInit() {
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

}
