import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

import { APP_CONFIG, AppConfig } from '../app-config.module';

import { Location, Fare } from '../models/location.model';

const DELIMITER = '/';
const SEARCH_PATH = '/airports';
const QUOTE_PATH = '/fares/';

export interface SearchResult {
  locations: Location[];
  total: number;
  pageNumber: number;
  totalPages: number;
}


@Injectable()
export class BackendService {

  constructor(private http: HttpClient,
              @Inject(APP_CONFIG) private config: AppConfig) {}

  search(term: string, page=1, size=5, lang='en') : Observable<SearchResult> {
    let parameters = new HttpParams({ fromObject: { page: page.toString(), size: size.toString(), lang: lang, term: term}});
    return this.http.get(this.config.apiEndpoint + SEARCH_PATH, {params: parameters}).pipe(
      map(response => {
        return {
          locations: response['_embedded']['locationList'],
          total: response['page']['totalElements'],
          pageNumber: response['page']['number'],
          totalPages: response['page']['totalPages']
        } as SearchResult;
      })
    );
  }

  locate(code: string) : Observable<Location> {
    let targetUri = this.config.apiEndpoint + SEARCH_PATH + DELIMITER + code;
    return this.http.get<Location>(targetUri);
  }

  getFare = function(origin: Location, destination: Location, currency='EUR') : Observable<Fare> {
  let parameters = new HttpParams({ fromObject: { currency: currency}});
    let targetUri = this.config.apiEndpoint + QUOTE_PATH + origin.code + DELIMITER + destination.code;
    return this.http.get(targetUri, {params: parameters});
  }

}