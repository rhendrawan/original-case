import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

import { APP_CONFIG, AppConfig } from '../app-config.module';

import { Metrics } from '../models/metrics.model';

const DELIMITER = '/';

const METRICS_PATH = '/actuator/metrics/';
const METRICS_PARAMS = new HttpParams({
  fromObject: {
  }
});

@Injectable()
export class MetricsService {

  constructor(private http: HttpClient,
              @Inject(APP_CONFIG) private config: AppConfig) {}

  getMetrics(name: string, tag: string, value: string) : Observable<Metrics> {
    if (name === '') {
      return of(new Metrics());
    }

    let params = {}
    if (tag && value) {
        params['params'] = METRICS_PARAMS.set('tag', tag + ':' + value)
    }
    let targetUri = this.config.apiEndpoint + METRICS_PATH + name;
    console.log(targetUri);
    console.log(params);

    return this.http.get(targetUri, params).pipe(
        map(response => response as Metrics)
      );
  }

  list() : Observable<string[]> {
    return this.http.get(this.config.apiEndpoint + METRICS_PATH).pipe(
       map(response => response as string[])
    );
  }


}