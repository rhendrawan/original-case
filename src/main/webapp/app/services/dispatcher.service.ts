import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Location, Fare } from '../models/location.model';

// https://angularfirebase.com/lessons/sharing-data-between-angular-components-four-methods/
@Injectable()
export class DispatcherService {

  DEFAULT_ORIGIN = new Location();
  DEFAULT_DESTINATION = new Location();

  private originSubject = new BehaviorSubject(this.DEFAULT_ORIGIN);
  currentOrigin = this.originSubject.asObservable();

  private destinationSubject = new BehaviorSubject(this.DEFAULT_DESTINATION);
  currentDestination = this.destinationSubject.asObservable();

  constructor() { }

  publishOrigin(origin: Location) {
    this.originSubject.next(origin);
  }

  publishDestination(destination: Location) {
    this.destinationSubject.next(destination);
  }

}