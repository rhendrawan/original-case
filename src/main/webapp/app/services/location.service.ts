import { Injectable, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

import { BehaviorSubject, Observable, of, Subject, defer, from, concat, EMPTY, timer } from 'rxjs';
import { debounceTime, delay, switchMap, tap, mergeMap, mapTo, map, toArray } from 'rxjs/operators';


import { BackendService, SearchResult } from './backend.service'
import { Location } from '../models/location.model';

export type SortDirection = 'asc' | 'desc' | '';


interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function getValue(location: Location, column: string) {
  return location[column] ||
    (column === 'city') ? location.parent.name :
    (column === 'country') ? location.parent.parent.name : '';
}

function sort(locations: Location[], column: string, direction: string): Location[] {
  if (direction === '') {
    return locations;
  } else {
    return [...locations].sort((a, b) => {
      const res = compare(getValue(a, column), getValue(b, column));
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(location: Location, term: string, pipe: PipeTransform) {
  return location.description.toLowerCase().includes(term.toLowerCase());
}

@Injectable({providedIn: 'root'})
export class LocationService {

  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _locations$ = new BehaviorSubject<Location[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  private _cache: Location[] = [];

  constructor(private backend: BackendService, private pipe: DecimalPipe) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._locations$.next(result.locations);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get locations$() { return this._locations$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() : string { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); this._resetPageNumber(); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); this._resetPageNumber(); this._resetCache(); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); this._resetPageNumber(); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); this._resetPageNumber(); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _resetPageNumber() {
    this.page = 1;
  }

  private _resetCache() {
    this._cache = [];
  }

  private _search(): Observable<SearchResult> {
    // Since the backend does not do their own sorting, we'll have to perform sorting against our own internal list.
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;
    if (sortColumn && sortDirection) {
      // 1. perform search and cache result to local memory.
      return this._getSearchResults(searchTerm, pageSize).pipe(
        map(locations => {

          // 2. sort cached result locally
          let sortedLocations = sort(locations, sortColumn, sortDirection);

          // 3. paginate countries from the list
          let paginatedLocations = sortedLocations.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
          return {
            locations: paginatedLocations,
            total: locations.length,
            pageNumber: page,
            totalPages: Math.ceil(locations.length / pageSize)
          } as SearchResult;
        })
      );
    }

    // if no sorting of any kind is specified, then we can directly use the backend service
    return this.backend.search(searchTerm, page, pageSize);
  }

  private _getSearchResults(searchTerm: string, pageSize: number) : Observable<Location[]> {
    if (this._cache.length > 0) {
      return of(this._cache);
    } else {
      return this._getLocations(searchTerm, 1, pageSize).pipe(
        toArray(), // collect *all* location into a single array, FWIW max elements = 1048 elements.
        tap((locations) => {this._cache = locations; console.log(this._cache)})
      );
    }
  }

  // https://stackoverflow.com/a/35494766/1271086
  private _getLocations(searchTerm: string, pageNumber: number, pageSize: number) : Observable<Location> {
    return defer(() => this.backend.search(searchTerm, pageNumber, pageSize))
      .pipe(
        mergeMap(result => {
          const result$ = from(result.locations);  // slightly less confusing to just use singular item here.
          const next$ = (pageNumber < result.totalPages) ? this._getLocations(searchTerm, pageNumber + 1, pageSize) : EMPTY;
          return concat(result$, next$);
        })
    );
  }

}