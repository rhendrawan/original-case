import { Component, OnInit } from '@angular/core';

import { Metrics, Measurement } from '../models/metrics.model';
import { MetricsService } from '../services/metrics.service';

const DEFAULT_TEXT = '---------';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [MetricsService],
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  metricNames: string[] = [
    "http.server.requests",
    "jvm.memory.max",
    "jvm.buffer.memory.used",
    "system.cpu.usage"
  ];
  selectedMetric: string = "http.server.requests";

  tagNames: string[] = [];
  selectedTag: string = DEFAULT_TEXT;

  tagValues: string[] = [];
  selectedValue: string = DEFAULT_TEXT;

  metrics: Metrics = null;

  measurements: Measurement[] = [];

  constructor(private metricService : MetricsService) { }

  ngOnInit() {
    this.loadMetric(this.selectedMetric);
  }

  loadMetric(metricName: string) {
    this.selectedMetric = metricName;
    this.tagNames = []
    this.selectedTag = DEFAULT_TEXT;
    this.tagValues = [];
    this.selectedValue = DEFAULT_TEXT;
    this.getMetrics(metricName, null, null);
  }

  loadTag(tagName: string) {
    this.selectedTag = tagName;
    this.tagValues = [];
    this.selectedValue = DEFAULT_TEXT;
    this.metrics.availableTags.forEach((tag) => {
      if (tag.tag === tagName) {
        this.tagValues = tag.values;
      }
    });
  }

  loadValue(tagValue: string) {
    this.selectedValue = tagValue;
    this.getMetrics(this.selectedMetric, this.selectedTag, this.selectedValue);
  }

  getMetrics(metricName, tagName, tagValue) {
    if (metricName !== DEFAULT_TEXT && tagName !== DEFAULT_TEXT && tagValue !== DEFAULT_TEXT) {
      this.metricService.getMetrics(metricName, tagName, tagValue)
        .subscribe(metrics => {
            this.metrics = metrics;
            this.measurements = metrics.measurements;
            this.tagNames = metrics.availableTags.map((tag) => tag.tag);
        });
    }
  }


}
