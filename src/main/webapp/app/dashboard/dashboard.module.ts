import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpClientModule, NgbModule ],
  declarations: [ DashboardComponent ],
  exports: [ DashboardComponent ],
  bootstrap: [ DashboardComponent ]
})

export class DashboardModule {}
